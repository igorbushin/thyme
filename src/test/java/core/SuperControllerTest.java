package core;

import javafx.scene.layout.VBox;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SuperControllerTest {

    @Test
    void loadFromFXML() {
        SuperController testController = new SuperController<>(VBox::new, SuperModel::new);
        testController.loadFromFXML("TestView");
        assertNotNull(testController.getView());
        assertNotNull(testController.getModel());
    }
}