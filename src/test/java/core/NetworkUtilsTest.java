package core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NetworkUtilsTest {

    private boolean result;

    @Test
    void isValidIpAddress() {
        /*result = NetworkUtils.isValidIpAddress("ya.ru");
        assertTrue(result);*/

        result = NetworkUtils.isValidIpAddress("127.0.0.1");
        assertTrue(result);

        result = NetworkUtils.isValidIpAddress("144351513");
        assertFalse(result);
    }

    @Test
    void isReachableIpAddress() {
        result = NetworkUtils.isReachableIpAddress("127.0.0.1", 100);
        assertTrue(result);

        result = NetworkUtils.isReachableIpAddress("144351513", 100);
        assertFalse(result);
    }
}