package core;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DataUtilsTest {

    class TestModel implements SavableModel {

        @Override
        public ArrayList<Object> getData() {
            ArrayList<Object> data = new ArrayList<>();
            data.add("abc");
            data.add(42);
            return data;
        }

        @Override
        public void setData(ArrayList<Object> data) {
            assertEquals(data.get(0), "abc");
            assertEquals(data.get(1), 42);
        }
    }
    @Test
    void complexTest() throws Exception {
        DataUtils.getInstance().dataPath = "./testData";
        DataUtils.getInstance().addModel(new TestModel());
        DataUtils.getInstance().addModel(new TestModel());
        DataUtils.getInstance().saveData();
        DataUtils.getInstance().loadData();
    }

}