package core;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class CheckUtilsTest {

    private ObjectProperty<String> notification;
    private boolean result;

    @BeforeEach
    void setup(){
        notification = new SimpleObjectProperty<>("");
    }

    @Test
    void isIntegerInput() {
        result = CheckUtils.isIntegerInput("32", notification);
        assertEquals(notification.get(), StringUtils.OperationComplete);
        assertTrue(result);

        result = CheckUtils.isIntegerInput("abs", notification);
        assertEquals(notification.get(), StringUtils.InvalidInput);
        assertFalse(result);

        result = CheckUtils.isIntegerInput("", notification);
        assertEquals(notification.get(), StringUtils.InvalidInput);
        assertFalse(result);
    }

    @Test
    void isSomethingSelected() {
        result = CheckUtils.isSomethingSelected(-1, Optional.of(notification));
        assertEquals(notification.get(), StringUtils.NothingSelected);
        assertFalse(result);

        result = CheckUtils.isSomethingSelected(0, Optional.of(notification));
        assertEquals(notification.get(), StringUtils.OperationComplete);
        assertTrue(result);

        result = CheckUtils.isSomethingSelected(1, Optional.of(notification));
        assertEquals(notification.get(), StringUtils.OperationComplete);
        assertTrue(result);
    }

    @Test
    void isFilledInput() {
        result = CheckUtils.isFilledInput("", Optional.of(notification));
        assertEquals(notification.get(), StringUtils.EmptyInput);
        assertFalse(result);

        result = CheckUtils.isFilledInput("abc", Optional.of((notification)));
        assertEquals(notification.get(), StringUtils.OperationComplete);
        assertTrue(result);
    }
}