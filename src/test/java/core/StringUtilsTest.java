package core;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class StringUtilsTest {

    @Test
    void convertToString() {
        assertEquals(StringUtils.convertToString(),"");
        assertEquals(StringUtils.convertToString("one"), "[one]");
        assertEquals(StringUtils.convertToString("one", "two", "three"), "[one][two][three]");
    }

    @Test
    void parseFromString() {
        assertEquals(StringUtils.parseFromString(""), new ArrayList<>());
        assertArrayEquals(StringUtils.parseFromString("[one]").toArray(), new String [] {"one"});
        assertArrayEquals(StringUtils.parseFromString("[one][two][three]").toArray(), new String [] {"one", "two", "three"});
    }
}