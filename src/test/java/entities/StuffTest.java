package entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StuffTest {

    private Stuff stuff1, stuff2;

    @BeforeEach
    void setup(){
        stuff1 = new Stuff("[name][32]");
        stuff2 = new Stuff("name", 32);
        assertNotNull(stuff1);
        assertNotNull(stuff2);
    }

    @Test
    void getName() {
        assertEquals(stuff1.getName(), "name");
        assertEquals(stuff2.getName(), "name");
    }

    @Test
    void getPrice() {
        assertEquals(stuff1.getPrice().intValue(), 32);
        assertEquals(stuff2.getPrice().intValue(), 32);
    }

    @Test
    void toStringTest() {
        assertEquals(stuff1.toString(), "[name][32]");
        assertEquals(stuff2.toString(), "[name][32]");
    }
}