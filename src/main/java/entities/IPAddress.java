package entities;

import core.NetworkUtils;
import core.StringUtils;

import java.io.Serializable;

public class IPAddress implements Serializable {

    private static final String REACHABLE = "+";
    private static final String UNREACHABLE = "-";
    private static final String UNKNOWN = "?";
    private String address;
    private String name;
    private String isReachable;

    public IPAddress(String address, String name) {
        this.address = address;
        this.name = name;
        this.isReachable = UNKNOWN;
    }

    @Override
    public String toString() {
        return StringUtils.convertToString(isReachable, address, name);
    }

    public void updateReachable() {
        boolean isReachable = NetworkUtils.isReachableIpAddress(address, 1000);
        this.isReachable = isReachable ? REACHABLE : UNREACHABLE;
    }
}
