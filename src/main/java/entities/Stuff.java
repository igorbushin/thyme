package entities;

import core.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;

public class Stuff implements Serializable {

    private String name;
    private Integer price;

    public Stuff(String stuff) {
        try {
            ArrayList<String> strings = StringUtils.parseFromString(stuff);
            name = strings.get(0);
            price = Integer.parseInt(strings.get(1));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Stuff(String name, Integer price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return StringUtils.convertToString(name, price.toString());
    }
}
