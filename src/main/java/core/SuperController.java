package core;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.util.function.Supplier;

public class SuperController <ViewType extends Parent, ModelType extends SuperModel> {

    protected ViewType view;

    protected ModelType model;

    public ViewType getView() {
        return view;
    }

    public ModelType getModel() {
        return model;
    }

    public SuperController (Supplier<ViewType> viewSupplier, Supplier<ModelType> modelSupplier) {
        view = viewSupplier.get();
        model = modelSupplier.get();
    }

    protected void loadFromFXML(String name) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/" + name + ".fxml"));
        fxmlLoader.setRoot(view);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
