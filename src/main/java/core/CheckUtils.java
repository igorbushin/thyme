package core;

import javafx.beans.property.ObjectProperty;

import java.util.Optional;

public class CheckUtils {

    public static boolean isIntegerInput(String input, ObjectProperty<String> notification) {
        try {
            Integer.parseInt(input);
            notification.set(StringUtils.OperationComplete);
            return true;
        }
        catch (Exception ignored) {
            notification.set(StringUtils.InvalidInput);
            return false;
        }
    }

    public static boolean isSomethingSelected(int selectedIndex, Optional<ObjectProperty<String>> notification) {
        boolean isSomethingSelected = selectedIndex >= 0;
        notification.ifPresent(stringObjectProperty ->
                stringObjectProperty.set(isSomethingSelected ? StringUtils.OperationComplete : StringUtils.NothingSelected)
        );
        return isSomethingSelected;
    }

    public static boolean isFilledInput(String input, Optional<ObjectProperty<String>> notification) {
        boolean isEmptyInput = input.isEmpty();
        notification.ifPresent(stringObjectProperty -> {
            stringObjectProperty.set(isEmptyInput ? StringUtils.EmptyInput : StringUtils.OperationComplete);
        });
        return !isEmptyInput;
    }

}
