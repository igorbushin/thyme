package core;

import java.net.InetAddress;

//TODO: use another thread or regexp
public class NetworkUtils {
    public static boolean isValidIpAddress(String ip) {
        try {
            String address = InetAddress.getByName(ip).toString();
            boolean isEqualPrefix = address.startsWith(ip);
            boolean isEqualSuffix = address.endsWith(ip);
            return isEqualPrefix || isEqualSuffix;
        }
        catch (Exception e) {
            return false;
        }
    }

    public static boolean isReachableIpAddress(String ip, int timeout) {
        try {
            return InetAddress.getByName(ip).isReachable(timeout);
        }
        catch (Exception e) {
            return false;
        }
    }
}
