package core;

import java.util.ArrayList;

public interface SavableModel {

    ArrayList<Object> getData();

    void setData(ArrayList<Object> data);
}
