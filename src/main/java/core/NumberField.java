package core;

import javafx.scene.control.TextField;

public class NumberField extends TextField {

    public NumberField(String text) {
        super();
        textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.isEmpty()) {
                try {
                    long longValue = Integer.parseInt(newValue);
                } catch (Exception e) {
                    clear();
                    setText(oldValue);
                }
            }
        });
        setText(text);
    }

    public int getNumber() {
        return Integer.parseInt(getText());
    }
}
