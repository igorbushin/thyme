package core;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public abstract class BackgroundTask implements AutoCloseable{
    //TODO:states instead properties
    public ObjectProperty<Boolean> isRun = new SimpleObjectProperty<>(false);
    public ObjectProperty<Boolean> shitHappens = new SimpleObjectProperty<>(false);

    private Thread thread;

    private boolean isRun(){
        return thread != null && thread.isAlive();
    }

    public void run(){
        if(isRun()) {
            kill();
        }
        thread = new Thread(() -> {
            try {
                isRun.setValue(true);
                taskBody();
                shitHappens.setValue(false);
                isRun.setValue(false);
            } catch (Exception e) { //include InterruptedException
                shitHappens.setValue(true);
                isRun.setValue(false);
            }
        });
        thread.start();
    }

    public void kill(){
        if(isRun())
            thread.interrupt();
    }

    protected abstract void taskBody() throws Exception;

    @Override
    public void close() {
        kill();
    }
}
