package core;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class DataUtils {
    private final static DataUtils ourInstance = new DataUtils();
    private final ArrayList<SavableModel> models = new ArrayList<>();
    protected String dataPath = "./data";

    public static DataUtils getInstance() {
        return ourInstance;
    }

    private DataUtils() {
    }

    public void addModel(SavableModel model) {
        models.add(model);
    }

    public void saveData() throws Exception {
        FileOutputStream fileOutputStream = new FileOutputStream(dataPath);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        for (SavableModel model : models) {
            try {
                objectOutputStream.writeObject(model.getData());
            }
            catch (Exception e) {
                System.out.println(model.getClass().getName() + "failed data save");
            }
        }
        objectOutputStream.close();
        fileOutputStream.close();
    }

    public void loadData() throws Exception {
        FileInputStream fileInputStream = new FileInputStream(dataPath);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        for (SavableModel model : models) {
            try {
                model.setData((ArrayList<Object>) objectInputStream.readObject());
            }
            catch (Exception e) {
                System.out.println(model.getClass().getName() + "failed data load");
            }
        }
        objectInputStream.close();
        fileInputStream.close();
    }
}
