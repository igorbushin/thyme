package core;

import java.util.ArrayList;

public class StringUtils {
    final static String OperationComplete = "Operation complete";
    final static String NothingSelected = "Nothing selected";
    final static String EmptyInput = "Empty input";
    public final static String InvalidInput = "Invalid input";

    public static String convertToString(String... args) {
        StringBuilder result = new StringBuilder();
        for (String string : args) {
            result.append("[").append(string).append("]");
        }
        return result.toString();
    }
    public static ArrayList<String> parseFromString(String string) {
        ArrayList<String> result = new ArrayList<>();
        String[] parts = string.split("]");
        for (String part : parts) {
            if(part.length() > 1)
                result.add(part.substring(1));
        }
        return result;
    }
}
