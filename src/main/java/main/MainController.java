package main;

import clicker.ClickerController;
import controlPanel.ControlPanelController;
import core.DataUtils;
import core.SuperController;
import core.SuperModel;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import moneyTracker.MoneyTrackerController;
import pinger.PingerController;

import java.io.Closeable;

public class MainController extends SuperController<TabPane, SuperModel> implements Closeable {

    private ClickerController clickerController = new ClickerController();

    public MainController() {
        super(TabPane::new, SuperModel::new);
        MoneyTrackerController moneyTrackerController = new MoneyTrackerController();
        ControlPanelController controlPanelController = new ControlPanelController();
        PingerController pingerController = new PingerController();
        addTab("clicker", clickerController.getView());
        addTab("pinger", pingerController.getView());
        addTab("money tracker", moneyTrackerController.getView());
        addTab("control panel", controlPanelController.getView());
        DataUtils dataUtils = DataUtils.getInstance();
        dataUtils.addModel(moneyTrackerController.getModel());
        dataUtils.addModel(pingerController.getModel());
        dataUtils.addModel(clickerController.getModel());
    }

    private void addTab(String tabName, Node content) {
        Tab newTab = new Tab(tabName);
        newTab.setClosable(false);
        newTab.setContent(content);
        view.getTabs().add(newTab);
    }

    @Override
    public void close() {
        clickerController.close();
    }
}
