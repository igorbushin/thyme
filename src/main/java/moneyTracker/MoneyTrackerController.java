package moneyTracker;

import core.SuperController;
import entities.Stuff;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class MoneyTrackerController extends SuperController<VBox, MoneyTrackerModel> {

    @FXML private ListView<Stuff> forPurchaseListView;
    @FXML private ListView<Stuff> purchasedListView;
    @FXML private Slider slider;
    @FXML private TextField stuffNameField;
    @FXML private TextField stuffPriceField;
    @FXML private Label forPurchaseLabel;
    @FXML private Label purchasedLabel;
    @FXML private Label notificationLabel;

    //TODO: рефакторинг
    public MoneyTrackerController() {
        super(VBox::new, MoneyTrackerModel::new);
        loadFromFXML("MoneyTrackerView");
        forPurchaseListView.itemsProperty().bind(model.forPurchaseList);
        purchasedListView.itemsProperty().bind(model.purchasedList);
        forPurchaseLabel.textProperty().bind(model.forPurchaseLabelText);
        purchasedLabel.textProperty().bind(model.purchasedLabelText);
        notificationLabel.textProperty().bind(model.notificationLabelText);

        forPurchaseListView.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.intValue() >= 0) {
                purchasedListView.getSelectionModel().clearSelection();
                slider.setValue(slider.getMin());
                Stuff stuff = forPurchaseListView.getSelectionModel().getSelectedItem();
                if(stuff != null) {
                    stuffNameField.setText(stuff.getName());
                    stuffPriceField.setText(stuff.getPrice().toString());
                }
            }
        });
        purchasedListView.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.intValue() >= 0) {
                forPurchaseListView.getSelectionModel().clearSelection();
                slider.setValue(slider.getMax());
            }
        });
        slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            boolean isSliderOnLeftSide = newValue.doubleValue() > 0.5;
            ListView<Stuff> listView = isSliderOnLeftSide ? forPurchaseListView : purchasedListView;
            model.moveStuff(isSliderOnLeftSide, listView.getSelectionModel().getSelectedIndex());
        });
        model.forPurchaseList.get().addListener((ListChangeListener<Stuff>) change -> model.updateForPurchaseLabelText());
        model.purchasedList.get().addListener((ListChangeListener<Stuff>) change -> model.updatePurchasedLabelText());
        model.updateForPurchaseLabelText();
        model.updatePurchasedLabelText();
    }

    @FXML private void addStuff(){
        model.addStuff(stuffNameField.getText(), stuffPriceField.getText());
    }

    @FXML private void editStuff(){
        int selectedIndex = forPurchaseListView.getSelectionModel().getSelectedIndex();
        model.editStuff(stuffNameField.getText(), stuffPriceField.getText(), selectedIndex);
    }

    @FXML private void deleteStuff(){
        model.deleteStuff(forPurchaseListView.getSelectionModel().getSelectedIndex());
    }

}
