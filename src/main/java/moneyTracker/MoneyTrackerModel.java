package moneyTracker;

import core.CheckUtils;
import core.SavableModel;
import core.SuperModel;
import entities.Stuff;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Optional;

class MoneyTrackerModel extends SuperModel implements SavableModel {

    final ObjectProperty<ObservableList<Stuff>> forPurchaseList = new SimpleObjectProperty<>(FXCollections.observableArrayList());
    final ObjectProperty<ObservableList<Stuff>> purchasedList = new SimpleObjectProperty<>(FXCollections.observableArrayList());
    final ObjectProperty<String> forPurchaseLabelText = new SimpleObjectProperty<>();
    final ObjectProperty<String> purchasedLabelText = new SimpleObjectProperty<>();
    final ObjectProperty<String> notificationLabelText = new SimpleObjectProperty<>();

    public void updatePurchasedLabelText() {
        Integer totalPriceInteger = 0;
        for (Stuff stuff : purchasedList.get()) {
            totalPriceInteger += stuff.getPrice();
        }
        purchasedLabelText.set("Purchased: " + totalPriceInteger.toString());
    }

    public void updateForPurchaseLabelText(){
        Integer totalPriceInteger = 0;
        for (Stuff stuff : forPurchaseList.get()) {
            totalPriceInteger += stuff.getPrice();
        }
        forPurchaseLabelText.set("For purchase: " + totalPriceInteger.toString());
    }

    void addStuff(String name, String price) {
        if(!CheckUtils.isIntegerInput(price, notificationLabelText)) return;
        if(!CheckUtils.isFilledInput(name, Optional.of(notificationLabelText))) return;
        if(!CheckUtils.isFilledInput(price, Optional.of(notificationLabelText))) return;
        forPurchaseList.get().add(new Stuff(name, Integer.parseInt(price)));
    }

    void editStuff(String name, String price, int selectedIndex) {
        if(!CheckUtils.isSomethingSelected(selectedIndex, Optional.of(notificationLabelText))) return;
        if(!CheckUtils.isFilledInput(name, Optional.of(notificationLabelText))) return;
        if(!CheckUtils.isFilledInput(price, Optional.of(notificationLabelText))) return;
        forPurchaseList.get().set(selectedIndex, new Stuff(name, Integer.parseInt(price)));
    }

    void deleteStuff(int selectedIndex) {
        if(!CheckUtils.isSomethingSelected(selectedIndex, Optional.of(notificationLabelText))) return;
        forPurchaseList.get().remove(selectedIndex);
    }

    void moveStuff(boolean fromForPurchase, int selectedIndex) {
        if(!CheckUtils.isSomethingSelected(selectedIndex, Optional.empty())) return;
        if(fromForPurchase) {
            Stuff stuff = forPurchaseList.get().get(selectedIndex);
            forPurchaseList.get().remove(selectedIndex);
            purchasedList.get().add(stuff);
        }
        else {
            Stuff stuff = purchasedList.get().get(selectedIndex);
            purchasedList.get().remove(selectedIndex);
            forPurchaseList.get().add(stuff);
        }
    }

    @Override
    public ArrayList<Object> getData() {
        ArrayList<Object> data = new ArrayList<>();
        data.add(new ArrayList<> (forPurchaseList.get()));
        data.add(new ArrayList<> (purchasedList.get()));
        return data;
    }

    @Override
    public void setData(ArrayList<Object> data) {
        forPurchaseList.get().clear();
        purchasedList.get().clear();

        forPurchaseList.get().addAll((ArrayList<Stuff>) data.get(0));
        purchasedList.get().addAll((ArrayList<Stuff>)data.get(1));
    }
}