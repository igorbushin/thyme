import main.MainController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

//TODO: переместить fxml файлы в пакеты
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        MainController mainController = new MainController();
        primaryStage.setScene(new Scene(mainController.getView(), 400, 300));
        primaryStage.setTitle("thyme");
        primaryStage.setOnCloseRequest(event -> mainController.close());
        primaryStage.show();
    }

}
