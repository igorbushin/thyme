package clicker;

import core.BackgroundTask;
import core.ImageUtils;
import core.SavableModel;
import core.SuperModel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.embed.swing.SwingFXUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import static java.awt.event.KeyEvent.*;

class ClickerModel extends SuperModel implements SavableModel {

    private Robot robot;
    public ClickerModel() {
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    private int getMouseCodeAndMoveByString(String str) {
        String[] strings = str.split("-");
        int mouseX = Integer.parseInt(strings[1]);
        int mouseY = Integer.parseInt(strings[2]);
        robot.mouseMove(mouseX, mouseY);
        switch(strings[0]) {
            case "ml": return BUTTON1_DOWN_MASK;
            case "mr": return BUTTON3_DOWN_MASK;
            default: return -1;
        }
    }

    private int getKeyCodeByString(String str) {
        if(str.length() == 1)
        {
            Character c0 = str.charAt(0);
            if('a' <= c0 && c0 <= 'z') {
                return VK_A + (c0 - 'a');
            }
            else if('0' <= c0 && c0 <= '9') {
                return VK_0 + (c0 - '0');
            }
            else if(c0 == '<') {
                return VK_LEFT;
            }
            else if(c0 == '>') {
                return VK_RIGHT;
            }
            else return -1;
        }
        else switch (str){
            case "/\\": return VK_UP;
            case "\\/": return VK_DOWN;
            case "esc": return VK_ESCAPE;
            case "enter": return VK_ENTER;
            case "space": return VK_SPACE;
            case "backspace": return VK_BACK_SPACE;
            case "delete": return VK_DELETE;
            case "tab": return VK_TAB;
            case "ctrl": return VK_CONTROL;
            case "shift": return VK_SHIFT;
            case "alt": return VK_ALT;
            default: return -1;
        }
    }

    private void pressKey(String str) {
        if(str.contains("-")){
            robot.mousePress(getMouseCodeAndMoveByString(str));
        }
        else {
            robot.keyPress(getKeyCodeByString(str));
        }
    }

    private void releaseKey(String str) {
        if(str.contains("-")){
            robot.mouseRelease(getMouseCodeAndMoveByString(str));
        }
        else {
            robot.keyRelease(getKeyCodeByString(str));
        }
    }

    private BufferedImage getImage(int x1, int y1, int x2, int y2) {
        int minX = Math.min(x1, x2);
        int minY = Math.min(y1, y2);
        int lenghtX = Math.max(x1, x2) - minX + 1;
        int lenghtY = Math.max(y1, y2) - minY + 1;

        return robot.createScreenCapture(new Rectangle(minX, minY, lenghtX, lenghtY));
    }

    private BufferedImage getImageByStr(String str) throws Exception {
        String[] strings = str.split("-");
        int x1 = Integer.parseInt(strings[1]);
        int y1 = Integer.parseInt(strings[2]);
        int x2 = Integer.parseInt(strings[3]);
        int y2 = Integer.parseInt(strings[4]);
        return getImage(x1, y1, x2, y2);
    }

    private void waitProc(String str) throws Exception {
        BufferedImage prevImage = getImageByStr(str);
        int count = 0;
        while(count < 10) {
            Thread.sleep(100);
            BufferedImage curImage = getImageByStr(str);
            if(ImageUtils.bufferedImagesEqual(prevImage, curImage)){
                count++;
            }
            else {
                count = 0;
            }
            prevImage = curImage;
        }
    }

    private void waitIDLE(String str) throws Exception {
        BufferedImage prevImage = getImageByStr(str);
        while(true) {
            Thread.sleep(100);
            BufferedImage curImage = getImageByStr(str);
            if(!ImageUtils.bufferedImagesEqual(prevImage, curImage)){
                break;
            }
        }
    }

    private void waitColor(String str) throws Exception {
        String[] strings = str.split("-");
        int x1 = Integer.parseInt(strings[1]);
        int y1 = Integer.parseInt(strings[2]);
        int rgb = Integer.parseInt(strings[3]);
        while (true) {
            Thread.sleep(100);
            BufferedImage image = getImage(x1, y1, x1, y1);
            if(image.getRGB(0,0) == -rgb) {
                break;
            }
        }
    }

    public ObjectProperty<javafx.scene.image.Image> image = new SimpleObjectProperty<>();
    public ObjectProperty<String> scriptText = new SimpleObjectProperty<>();
    public ObjectProperty<String> scriptCount = new SimpleObjectProperty<>();
    public ObjectProperty<String> mousePosition = new SimpleObjectProperty<>();
    public MouseTrackerTask mouseTrackerTask = new MouseTrackerTask();

    public ClickerTask task = new ClickerTask();

    @Override
    public ArrayList<Object> getData() {
        ArrayList<Object> data = new ArrayList<>();
        data.add(scriptText.get());
        data.add(scriptCount.get());
        return data;
    }

    @Override
    public void setData(ArrayList<Object> data) {
        scriptText.set((String)data.get(0));
        scriptCount.set((String)data.get(1));
    }
    public class MouseTrackerTask extends BackgroundTask {
        @Override
        protected void taskBody() throws Exception {
            while(true) {
                int mouseX = (int) MouseInfo.getPointerInfo().getLocation().getX();
                int mouseY = (int) MouseInfo.getPointerInfo().getLocation().getY();
                int d = 10;
                Rectangle area = new Rectangle(mouseX - d, mouseY - d, 2*d+1, 2*d+1);
                BufferedImage bufImage = robot.createScreenCapture(area);
                int rgb = bufImage.getRGB(d, d);
                for (int i = -1; i <= 1; ++i)
                    for (int j = -1; j <= 1; ++j)
                        if(i != 0 || j != 0) {
                            bufImage.setRGB(d + i, d + j, Color.BLACK.getRGB());
                        }
                mousePosition.setValue(" rgb:" + String.valueOf(-rgb) + "\n x:" + mouseX + "\n y:" + mouseY);
                image.setValue(SwingFXUtils.toFXImage(bufImage, null));
                Thread.sleep(100);
            }
        }

    }
    public class ClickerTask extends BackgroundTask {
        @Override
        protected void taskBody() throws Exception {
            //TODO:real language support
            String script = scriptText.get();
            script = script.toLowerCase();
            int count = Integer.parseInt(scriptCount.get());
            for (int j = 0; j < count; ++j) {
                String[] strings = script.split("\n");
                for (String string : strings) {
                    string = string.trim();
                    String[] codeAndComment = string.split("#");
                    String[] tokens = codeAndComment[0].split(" ");
                    int pause = Integer.parseInt(tokens[0]);
                    Thread.sleep(100);
                    for (int k = 0; k < pause; ++k) Thread.sleep(1000);
                    for (int i = 1; i < tokens.length; ++i) {
                        if(tokens[i].contains("wi")){
                            waitIDLE(tokens[i]);
                        }
                        else if(tokens[i].contains("wp")) {
                            waitProc(tokens[i]);
                        }
                        else if(tokens[i].contains("wc")) {
                            waitColor(tokens[i]);
                        }
                    }
                    for (int i = 1; i < tokens.length; ++i) {
                        if(!tokens[i].contains("wi") && !tokens[i].contains("wp") && !tokens[i].contains("wc"))
                            pressKey(tokens[i]);
                    }
                    Thread.sleep(100);
                    for (int i = 1; i < tokens.length; ++i) {
                        if(!tokens[i].contains("wi") && !tokens[i].contains("wp") && !tokens[i].contains("wc"))
                            releaseKey(tokens[i]);
                    }
                }
            }
        }

    }
}