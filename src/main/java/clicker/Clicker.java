package clicker;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class Clicker extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        ClickerController clickerController = new ClickerController();
        Scene scene = new Scene(clickerController.getView(), 400, 300);
        URL url = getClass().getResource("/spreadsheet.css");
        if (url == null) {
            System.out.println("Resource not found. Aborting.");
            System.exit(-1);
        }
        String css = url.toExternalForm();
        scene.getStylesheets().add(css);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Clicker");
        primaryStage.setOnCloseRequest(event -> clickerController.close());
        primaryStage.show();
    }

}
