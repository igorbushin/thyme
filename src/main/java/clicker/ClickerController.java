package clicker;

import core.NumberField;
import core.SuperController;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;

import java.io.Closeable;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClickerController extends SuperController<VBox, ClickerModel> implements Closeable {

    @FXML private HBox bottomContainer;
    @FXML private CodeArea codeArea;

    private static final String[] KEYWORDS = new String[]{
            "space", "esc", "enter", "alt", "tab",
            "ctrl", "shift", "ml", "mr", "wp", "wi", "wc",
            "/\\", "\\/", "<", ">" //TODO:fix
    };

    private static final String KEYWORD_PATTERN = "\\b(" + String.join("|", KEYWORDS) + ")\\b";
    private static final String COMMENT_PATTERN = "#[^\n]*";// + "|" + "/\\*(.|\\R)*?\\*/";
    private static final Pattern PATTERN = Pattern.compile(
            "(?<KEYWORD>" + KEYWORD_PATTERN + ")|(?<COMMENT>" + COMMENT_PATTERN + ")"
    );

    public ClickerController() {
        super(VBox::new, ClickerModel::new);
        loadFromFXML("ClickerView");

        codeArea.setParagraphGraphicFactory(LineNumberFactory.get(codeArea));
        codeArea.setPrefWidth(1000);
        codeArea.setPrefHeight(1000);
        codeArea.inputMethodRequestsProperty().addListener((observable, oldValue, newValue) -> {
            System.out.print("HO");
        });
        codeArea.setOnInputMethodTextChanged(event -> {
            System.out.print("HO");
        });
        codeArea.onInputMethodTextChangedProperty().addListener((observable, oldValue, newValue) -> {
            System.out.print("HO");
        });
        codeArea.textProperty().addListener((observable, oldValue, newValue) -> {
            codeArea.setStyleSpans(0, calcHighlighting(newValue));
        });
        model.scriptText.bind(codeArea.textProperty());


        NumberField countField = new NumberField("1");
        countField.setMaxWidth(100);
        model.scriptCount.bindBidirectional(countField.textProperty());

        Button runButton = new Button();
        runButton.setText("run");
        runButton.setOnMouseClicked(event -> {
            boolean isTaskRun = model.task.isRun.get();
            if (isTaskRun) {
                model.task.kill();
            } else {
                model.task.run();
            }
        });
        model.task.isRun.addListener(observable -> {
            boolean isTaskRun = model.task.isRun.get();
            boolean isShitHappens = model.task.shitHappens.get();
            if(isTaskRun) {
                Platform.runLater(() -> runButton.setText("stop"));
                Platform.runLater(() -> runButton.setStyle("-fx-background-color: #ff0000;"));
            }
            else {
                if(isShitHappens) {
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText("Что-то пошло не так");
                        alert.setContentText("Проверьте, что команды написаны правильно\n"
                                +"Проверьте, что количество запусков указано правильно\n");
                        alert.showAndWait();
                    });
                }
                Platform.runLater(() -> runButton.setText("run"));
                Platform.runLater(() -> runButton.setStyle("-fx-background-color: #00ff00;"));
            }
        });

        Button helpButton = new Button("?");
        helpButton.setOnMouseClicked(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("ml-x-y = левая кнопка мыши");
            alert.setHeaderText("Доступные клавиши");
            TextArea helpArea = new TextArea("0-9 a-z = цифры, буквы\n"
                    +"/\\ \\/ < > = клавиши вверх,вниз,влево,вправо\n"
                    +"ml-x-y mr-x-y = левая и правая клавиши мыши\n"
                    +"wp-x1-y1-x2-y2 = ожидание завершения изменений в области\n"
                    +"wi-x1-y1-x2-y2 = ожидание каких-либо изменений в области\n"
                    +"wc-x-y-rgb = ожидание цвета в точке х у\n"
                    +"space = пробел\n"
                    +"esc enter alt tab ctrl shift\n"
                    +"\n"
                    +"Пример скрипта:\n"
                    +"1 wi-0-0-50-50 #ожидание изменений в левом верхнем углу экрана\n"
                    +"1 ml-600-300 #клик левой кнопкой в точку с х=600 у=300\n"
                    +"1 ctrl a #выделяем все\n"
                    +"1 ctrl c #копируем\n"
                    +"1 /\\ #перемещаем курсор вверх\n"
                    +"1 ctrl v #вставляем\n"
                    +"1 ctrl z #отменяем\n"
            );
            helpArea.setEditable(false);
            helpArea.setWrapText(true);
            helpArea.setMaxWidth(Double.MAX_VALUE);
            helpArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(helpArea, Priority.ALWAYS);
            GridPane.setHgrow(helpArea, Priority.ALWAYS);
            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(helpArea, 0, 0);
            alert.getDialogPane().setExpandableContent(expContent);
            alert.getDialogPane().setExpanded(true);
            alert.showAndWait();
        });

        Label label = new Label();
        label.setText(" times ");
        runButton.setStyle("-fx-background-color:#00ff00;");

        Label mousePosition = new Label();
        model.mouseTrackerTask.run();
        model.mousePosition.addListener((observable, oldValue, newValue) -> {
            Platform.runLater(() -> mousePosition.setText(newValue));
        });

        ImageView imageView = new ImageView();
        imageView.setFitHeight(54);
        imageView.setFitWidth(54);
        imageView.setSmooth(false);
        imageView.imageProperty().bind(model.image);
        BorderPane imageViewWrapper = new BorderPane(imageView);
        imageViewWrapper.setStyle("-fx-border-color: black;\n" +
                "    -fx-border-style: solid;\n" +
                "    -fx-border-width: 1;");
        bottomContainer.getChildren().addAll(helpButton, runButton, countField, label, imageViewWrapper, mousePosition);
    }

    private StyleSpans<Collection<String>> calcHighlighting(String text) {
        StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();
        Matcher matcher = PATTERN.matcher(text);
        int lastKwEnd = 0;
        while(matcher.find()) {
            String styleClass =
                matcher.group("KEYWORD") != null ? "keyword" :
                matcher.group("COMMENT") != null ? "comment" :
                null;  assert styleClass != null;
            spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
            spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
            lastKwEnd = matcher.end();
        }
        spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
        return  spansBuilder.create();
    }

    @Override
    public void close() {
        model.task.kill();
        model.mouseTrackerTask.kill();
    }
}
