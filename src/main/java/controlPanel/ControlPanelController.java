package controlPanel;

import core.DataUtils;
import core.SuperController;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

//TODO: выбор файла
public class ControlPanelController extends SuperController<VBox, ControlPanelModel> {
    public ControlPanelController() {
        super(VBox::new, ControlPanelModel::new);
        loadFromFXML("ControlPanelView");
    }

    @FXML private void loadData() {
        try {
            DataUtils.getInstance().loadData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML private void saveData() {
        try {
            DataUtils.getInstance().saveData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
