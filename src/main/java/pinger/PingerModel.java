package pinger;

import core.*;
import entities.IPAddress;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Optional;

class PingerModel extends SuperModel implements SavableModel {

    final ObjectProperty<ObservableList<IPAddress>> ipList =  new SimpleObjectProperty<>(FXCollections.observableArrayList());
    final ObjectProperty<String> notificationLabelText = new SimpleObjectProperty<>();

    public void updateReachable() {
        ObservableList<IPAddress> IPsList = ipList.get();
        for (int i = 0; i < IPsList.size(); ++i) {
            IPAddress ipAddress = IPsList.get(i);
            ipAddress.updateReachable();
            IPsList.set(i, ipAddress);
        }
    }

    public void deleteIPatIndex(int selectedIndex) {
        if(!CheckUtils.isSomethingSelected(selectedIndex, Optional.of(notificationLabelText))) return;
        ipList.get().remove(selectedIndex);
    }

    public void addIP(String ip, String name) {
        if(!CheckUtils.isFilledInput(ip, Optional.of(notificationLabelText))) return;
        if(!CheckUtils.isFilledInput(name, Optional.of(notificationLabelText))) return;
        if(!NetworkUtils.isValidIpAddress(ip)) {
            notificationLabelText.setValue(StringUtils.InvalidInput);
            return;
        }
        ipList.get().add(new IPAddress(ip, name));
    }

    @Override
    public ArrayList<Object> getData() {
        ArrayList<Object> data = new ArrayList<>();
        data.add(new ArrayList<> (ipList.get()));
        return data;
    }

    @Override
    public void setData(ArrayList<Object> data) {
        ipList.get().clear();
        ipList.get().addAll((ArrayList<IPAddress>)data.get(0));
    }
}
