package pinger;

import core.SuperController;
import entities.IPAddress;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class PingerController extends SuperController<VBox, PingerModel>{

    @FXML private TextField ipTextField;
    @FXML private TextField nameTextField;
    @FXML private ListView<IPAddress> ipListView;
    @FXML private Button addButton;
    @FXML private Button delButton;
    @FXML private Button testButton;
    @FXML private Label notificationLabel;

    public PingerController() {
        super(VBox::new, PingerModel::new);
        loadFromFXML("PingerView");
        ipTextField.setPromptText("IP");
        nameTextField.setPromptText("Name");
        addButton.setText("Add");
        delButton.setText("Delete");
        testButton.setText("Test");
        ipListView.itemsProperty().bind(model.ipList);
        notificationLabel.textProperty().bind(model.notificationLabelText);
    }

    @FXML public void addIP(){
        String ip = ipTextField.getText();
        String name = nameTextField.getText();
        model.addIP(ip, name);
    }

    @FXML public void deleteIP(){
        model.deleteIPatIndex(ipListView.getSelectionModel().getSelectedIndex());
    }

    @FXML public void testIPs(){
        model.updateReachable();
    }
}
